package fr.epita.iamcore.tests;

import javax.inject.Inject;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.epita.iam.iamlog.IamLogger;
import fr.epita.iam.iamlog.IamLoggerManager;
import fr.epita.iamcore.datamodel.Identity;
import fr.epita.iamcore.services.dao.IdentityDAO;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/ApplicationContext.xml"})
public class TestSpringConfiguration {
	private IamLogger log = IamLoggerManager.getIamLogger(TestSpringConfiguration.class);
	
	@Inject
	@Qualifier("identitySample")
	private Identity identity;
	
	@Inject
	private IdentityDAO dao;
	
	@Test
	public void testConfig() {
		Assert.assertNotNull(identity);
		Assert.assertEquals("Mikaël Popowicz", this.identity.getDisplayName());
		log.debug("Display name is : " + this.identity.getDisplayName());
	}
	
	@Test
	public void testDao() {
		Assert.assertNotNull(dao);
	}
}
