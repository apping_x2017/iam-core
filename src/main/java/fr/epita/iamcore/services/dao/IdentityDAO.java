package fr.epita.iamcore.services.dao;

import fr.epita.iamcore.datamodel.Identity;

public interface IdentityDAO extends DAO<Identity> {}
