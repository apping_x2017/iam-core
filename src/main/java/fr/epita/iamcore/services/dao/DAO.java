package fr.epita.iamcore.services.dao;

import java.util.Collection;

public interface DAO<T> {
	
	/**
	 * Save the given entity
	 * @param entity the entity to save
	 */
	void save(T entity);
	
	/**
	 * Update the given entity
	 * @param entity the entity to update
	 */
	void update(T entity);
	
	/**
	 * Delete the given entity
	 * @param entity the entity to delete
	 */
	void delete(T entity);
	
	/**
	 * Get entity which has the given id
	 * @param id the identifier of the entity
	 * @return T
	 */
	T getById(Object id);
	
	/**
	 * Search entity of type {T} with criteria
	 * @param criteria
	 * @return Collection<T> of entity which correspond to the criteria
	 */
	Collection<T> search(T criteria);
}
