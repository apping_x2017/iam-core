package fr.epita.iamcore.datamodel;

public class Identity {
	private String displayName;

	public final String getDisplayName() {
		return displayName;
	}

	public final void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
